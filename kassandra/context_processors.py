from django.conf import settings


def general_environment(request):
    """ Pass variables from settings to templates
    """
    return {
        "instance_name": settings.INSTANCE_NAME,
        "debug": settings.DEBUG,
        "environment": settings.ENVIRONMENT,
    }
