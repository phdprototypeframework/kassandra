from .base import *  # noqa

ENVIRONMENT = "docker"

SECRET_KEY = os.environ.get(  # noqa
    "SECRET_KEY", "n(41@8chw-#rj+12fprgjbs^9+uuzz=s8+1-cfx3#j+hmwns++"
)
DEBUG = bool(int(os.environ.get("DEBUG", 1)))  # noqa

ALLOWED_HOSTS = ["*"]
