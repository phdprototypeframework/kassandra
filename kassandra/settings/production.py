from .base import *  # noqa

ENVIRONMENT = "production"

SECRET_KEY = os.environ["SECRET_KEY"]  # noqa
DEBUG = False

ALLOWED_HOSTS = ["*"]
