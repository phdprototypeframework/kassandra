import pickle

import tensorflow.keras as keras
from tensorflow.keras.preprocessing import text


class TextPreprocessor:
    """ Create training data matrix
    """

    def __init__(self, vocab_size):
        self.vocab_size = vocab_size
        self.tokenizer = None

    def create_tokenizer(self, text_list):
        tokenizer = text.Tokenizer(num_words=self.vocab_size)
        tokenizer.fit_on_texts(text_list)
        self.tokenizer = tokenizer

    def transform_text(self, text_list):
        text_matrix = self.tokenizer.texts_to_matrix(text_list)
        return text_matrix


class CustomModelPrediction:
    """ Perform predictions
    """

    def __init__(self, model, processor):
        self.model = model
        self.processor = processor

    def predict(self, instances, **kwargs):
        preprocessed_data = self.processor.transform_text(instances)
        predictions = self.model.predict(preprocessed_data)
        return predictions.tolist()

    @classmethod
    def from_path(cls, multi_hot_encode_model_file, keras_model_file):
        model = keras.models.load_model(keras_model_file)

        with open(multi_hot_encode_model_file, "rb") as f:
            processor = pickle.load(f)

        return cls(model, processor)
