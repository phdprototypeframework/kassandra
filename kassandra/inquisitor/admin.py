from django.contrib import admin

from .models import PredictedUseCase, TextDocument


@admin.register(PredictedUseCase)
class PredictedUseCaseAdmin(admin.ModelAdmin):
    list_display = ("name",)
    search_fields = ("name",)


@admin.register(TextDocument)
class TextDocumentAdmin(admin.ModelAdmin):
    list_display = (
        "text",
        "predicted_use_case",
        "accurate_use_case",
        "expected_use_case",
    )
    search_fields = (
        "text",
        "predicted_use_case__name",
    )
