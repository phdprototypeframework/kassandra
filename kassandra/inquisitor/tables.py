from django.shortcuts import reverse
from django.utils.html import format_html

import django_tables2 as tables

from .models import TextDocument


class PredictionsTable(tables.Table):
    class Meta:
        attrs = {
            "class": "table table-borderless",
        }

    review_text = tables.Column(
        empty_values=(), orderable=False, verbose_name="Review Text"
    )
    predicted_use_case = tables.Column(
        orderable=False, verbose_name="Predicted Use Case"
    )
    prediction_accuracy = tables.TemplateColumn(
        template_name="prediction_accuracy.html",
        orderable=False,
        verbose_name="Prediction Accuracy",
    )

    def render_review_text(self, record):
        return record["text_document"].text


class TextDocumentTable(tables.Table):
    class Meta:
        model = TextDocument
        fields = (
            "text",
            "predicted_use_case",
            "accurate_use_case",
            "expected_use_case",
            "perform_prediction",
        )
        attrs = {
            "class": "table table-borderless",
        }

    text = tables.Column(orderable=False, verbose_name="Review Text")
    predicted_use_case = tables.Column(
        empty_values=(), verbose_name="Predicted Use Case"
    )
    accurate_use_case = tables.Column(
        empty_values=(), verbose_name="Prediction Accurate"
    )
    expected_use_case = tables.Column(empty_values=(), verbose_name="Expected Use Case")
    perform_prediction = tables.Column(
        empty_values=(), orderable=False, verbose_name=""
    )

    def render_predicted_use_case(self, record):
        if record.predicted_use_case is None:
            return "No Prediction"

        return record.predicted_use_case.name

    def render_accurate_use_case(self, record):
        if record.accurate_use_case is None:
            return "Not set"

        return "Yes" if record.accurate_use_case is True else "No"

    def render_expected_use_case(self, record):
        if record.accurate_use_case is None or record.accurate_use_case is True:
            return ""

        if record.expected_use_case is not None and record.expected_use_case != "":
            return record.expected_use_case

        return format_html(
            "<a class='btn btn-outline-dark' href='{}'>Submit</a>",
            reverse("inquisitor:expected_prediction", args=(record.id,)),
        )

    def render_perform_prediction(self, record):
        if record.predicted_use_case is not None:
            return ""

        return format_html(
            "<a class='btn btn-outline-primary' href='{}'>Predict</a>",
            reverse("inquisitor:predictions", args=(record.id,)),
        )
