from django.urls import path

from . import views

app_name = "inquisitor"

urlpatterns = [
    path("", views.index, name="index"),
    path("documents", views.text_documents, name="text_documents"),
    path("predictions/<int:text_document_id>", views.predictions, name="predictions"),
    path(
        "predictions/store/<int:text_document_id>",
        views.store_predictions,
        name="store_predictions",
    ),
    path(
        "predictions/expected/<int:text_document_id>",
        views.expected_prediction,
        name="expected_prediction",
    ),
]
