from django.apps import AppConfig


class InquisitorConfig(AppConfig):
    name = "inquisitor"
