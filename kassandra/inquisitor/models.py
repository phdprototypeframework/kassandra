from django.db import models


class PredictedUseCase(models.Model):
    class Meta:
        pass

    name = models.CharField(max_length=255, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class TextDocument(models.Model):
    class Meta:
        get_latest_by = "pk"

    text = models.TextField()
    predicted_use_case = models.ForeignKey(
        PredictedUseCase, on_delete=models.PROTECT, null=True, blank=True
    )
    accurate_use_case = models.BooleanField(null=True)
    expected_use_case = models.CharField(max_length=100, blank=True, default="")
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.id)
