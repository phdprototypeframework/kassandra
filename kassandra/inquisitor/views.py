import json
import logging
import string
from os import environ as os_environ, path as os_path

from django.contrib import messages
from django.db import transaction
from django.http import HttpResponseBadRequest, HttpResponseServerError
from django.shortcuts import get_object_or_404, redirect, render, reverse

from django_tables2 import RequestConfig
from review_analyzer import Review
from sklearn.preprocessing import MultiLabelBinarizer

from .classifiers import CustomModelPrediction
from .forms import ExpectedPredictionForm, TextDocumentForm
from .models import PredictedUseCase, TextDocument
from .tables import PredictionsTable, TextDocumentTable

LOGGER = logging.getLogger(__name__)

NO_PREDICTIONS_ERROR_MESSAGE = "Unable to produce predictions. Please try again"


def index(request):
    """ Index
    """
    if request.method == "POST":
        form = TextDocumentForm(request.POST)

        if form.is_valid():
            with transaction.atomic():
                text_document = form.save()

                messages.success(request, "Thank you for your submission")
                return redirect(
                    reverse("inquisitor:predictions", args=(text_document.id,))
                )
    else:
        form = TextDocumentForm()

    return render(request, "index.html", {"text_document_form": form})


def text_documents(request):
    """ Retrieve text documents
    """
    queryset = TextDocument.objects.select_related("predicted_use_case")

    table = TextDocumentTable(queryset)
    RequestConfig(request, paginate={"per_page": 10}).configure(table)

    return render(request, "text_documents.html", {"table": table})


def predictions(request, text_document_id):
    """ Analyse text documents and predict use cases
    """
    document = {}
    table = None

    expected_files = [
        "KERAS_MODEL_FILE",
        "MULTI_HOT_ENCODE_MODEL_FILE",
        "TAGS_LIST_FILE",
    ]
    missing_file = False

    for expected_file in expected_files:
        if expected_file not in os_environ:
            LOGGER.error("%s not set", expected_file)
            missing_file = True

    if missing_file:
        messages.error(request, NO_PREDICTIONS_ERROR_MESSAGE)
    else:
        text_document = get_object_or_404(TextDocument, pk=int(text_document_id))

        multi_hot_encode_model_file = os_environ["MULTI_HOT_ENCODE_MODEL_FILE"]
        keras_model_file = os_environ["KERAS_MODEL_FILE"]
        tags_list_file = os_environ["TAGS_LIST_FILE"]

        # check if files exist
        if not all(
            [
                os_path.exists(multi_hot_encode_model_file),
                os_path.exists(keras_model_file),
                os_path.exists(tags_list_file),
            ]
        ):
            LOGGER.error("Model files and tags list file must be provided")
            return HttpResponseServerError(
                "Unexpected error. Please contact system administrator"
            )

        tag_encoder = multi_hot_encode_tags(tags_list_file)
        features = nlp_analysis(text_document.text)
        classifier = CustomModelPrediction.from_path(
            multi_hot_encode_model_file, keras_model_file
        )
        predictions = classifier.predict([features])

        document["text_document"] = text_document
        document["features"] = features
        document["predicted_use_case"] = predict_use_case(predictions, tag_encoder)

        table = PredictionsTable([document])
        RequestConfig(request, paginate={"per_page": 10}).configure(table)

    return render(request, "predictions.html", {"table": table})


def multi_hot_encode_tags(tags_list_file):
    """ Extract tags list from file.
    Multi hot encode tags list
    """
    with open(tags_list_file, "r") as fhandle:
        tags_list = json.load(fhandle)["tags_list"]

    # multi hot encode tags list
    tag_encoder = MultiLabelBinarizer()
    tag_encoder.fit_transform(tags_list)

    return tag_encoder


def nlp_analysis(text):
    """ Perform natural language processing (NLP) on text
    """
    # remove punctuation from text
    review_text = text.translate(str.maketrans("", "", string.punctuation))
    # perform NLP on review text
    review = Review(review_text, preprocess=True)
    # lemmatize features
    features = " ".join(review.lemmatize(review.features))

    return features


def predict_use_case(predictions, tag_encoder):
    """ Predict use case
    """
    predicted_use_case = "unknown"

    for i in range(len(predictions)):
        for idx, val in enumerate(predictions[i]):
            if val > 0.6:
                predicted_use_case = tag_encoder.classes_[idx]
                break

    return predicted_use_case


def store_predictions(request, text_document_id):
    """ Store predictions if user determines them
    to be accurate
    """
    missing_key = False
    for key in ["use_case", "accurate"]:
        if key not in request.GET:
            LOGGER.error("%s not present", key)
            missing_key = True

    if missing_key:
        return HttpResponseBadRequest("Error: Missing data")

    accurate = True if request.GET["accurate"] == "true" else False

    text_document = get_object_or_404(TextDocument, pk=int(text_document_id))
    predicted_use_case, _ = PredictedUseCase.objects.get_or_create(
        name=request.GET["use_case"]
    )
    text_document.predicted_use_case = predicted_use_case
    text_document.accurate_use_case = accurate
    text_document.save()

    return redirect(reverse("inquisitor:text_documents"))


def expected_prediction(request, text_document_id):
    """ Store expected use case if predicted use case
    is inaccurate
    """
    text_document = get_object_or_404(TextDocument, pk=int(text_document_id))

    if request.method == "POST":
        form = ExpectedPredictionForm(request.POST, instance=text_document)

        if form.is_valid():
            with transaction.atomic():
                text_document = form.save()

                messages.success(request, "Thank you for your submission")
                return redirect(reverse("inquisitor:text_documents"))
    else:
        form = ExpectedPredictionForm(instance=text_document)

    return render(
        request,
        "expected_prediction.html",
        {"expected_prediction_form": form, "text_document": text_document},
    )
