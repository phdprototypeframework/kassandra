from django import forms

from .models import TextDocument


class TextDocumentForm(forms.ModelForm):
    class Meta:
        model = TextDocument
        fields = ("text",)
        labels = {
            "text": "Review Text",
        }
        widgets = {
            "text": forms.Textarea(attrs={"cols": 120, "rows": 15}),
        }
        help_texts = {
            "text": "Please enter a review",
        }


class ExpectedPredictionForm(forms.ModelForm):
    class Meta:
        model = TextDocument
        fields = (
            "text",
            "expected_use_case",
        )
        labels = {
            "text": "Review Text",
            "expected_use_case": "Expected Prediction",
        }
        widgets = {
            "text": forms.Textarea(
                attrs={
                    "cols": 120,
                    "rows": 15,
                    "class": "disabled",
                    "readonly": "readonly",
                }
            ),
        }
        help_texts = {
            "expected_use_case": "Please enter the expected prediction",
        }

    def clean(self):
        """ Validate form data
        """
        cleaned_data = super().clean()

        if cleaned_data["expected_use_case"] == "":
            self.add_error("expected_use_case", "Please provide an expected prediction")

        return cleaned_data
