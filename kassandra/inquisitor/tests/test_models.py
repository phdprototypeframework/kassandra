from django.test import TestCase

from kassandra.inquisitor.models import PredictedUseCase, TextDocument


class ModelsTestCase(TestCase):
    def setUp(self):
        self.predicted_use_case = PredictedUseCase.objects.create(name="run")
        self.current_predicted_use_case_count = PredictedUseCase.objects.count()

        self.text_doc = TextDocument.objects.create(text="hello")
        self.current_text_doc_count = TextDocument.objects.count()

    def test_create_predicted_use_case(self):
        predicted_use_case = PredictedUseCase.objects.create(name="swim")

        self.assertEqual("swim", predicted_use_case.name)
        self.assertEqual(
            1, PredictedUseCase.objects.count() - self.current_predicted_use_case_count
        )

    def test_delete_predicted_use_case(self):
        self.predicted_use_case.delete()

        with self.assertRaises(PredictedUseCase.DoesNotExist):
            PredictedUseCase.objects.get(name="run")

        self.assertEqual(0, PredictedUseCase.objects.count())

    def test_edit_predicted_use_case(self):
        self.predicted_use_case.name = "walk"
        self.predicted_use_case.save()

        self.assertEqual("walk", self.predicted_use_case.name)
        self.assertEqual(1, PredictedUseCase.objects.count())

    def test_create_text_document(self):
        text_doc = TextDocument.objects.create(text="review")

        self.assertEqual("review", text_doc.text)
        self.assertEqual(1, TextDocument.objects.count() - self.current_text_doc_count)

    def test_create_text_document_with_predicted_use_case(self):
        text_doc = TextDocument.objects.create(
            text="review", predicted_use_case=PredictedUseCase.objects.get(name="run")
        )

        self.assertEqual("review", text_doc.text)
        self.assertEqual(1, TextDocument.objects.count() - self.current_text_doc_count)
        self.assertEqual("run", text_doc.predicted_use_case.name)

    def test_create_multiple_text_documents(self):
        text_docs = ["arrow", "mulan"]

        for text_doc in text_docs:
            TextDocument.objects.create(text=text_doc)

        self.assertEqual(
            text_docs,
            [
                text_doc.text
                for text_doc in TextDocument.objects.filter(
                    text__in=text_docs,
                ).order_by("pk")
            ],
        )
        self.assertEqual(2, TextDocument.objects.count() - self.current_text_doc_count)

    def test_delete_text_document(self):
        self.text_doc.delete()

        with self.assertRaises(TextDocument.DoesNotExist):
            TextDocument.objects.get(text="hello")

        self.assertEqual(0, TextDocument.objects.count())

    def test_remove_predicted_use_case_from_text_document(self):
        text_doc = TextDocument.objects.create(
            text="review", predicted_use_case=PredictedUseCase.objects.get(name="run")
        )
        text_doc.predicted_use_case = None
        text_doc.save()

        self.assertEqual(None, text_doc.predicted_use_case)

    def test_edit_text_document(self):
        self.text_doc.text = "world"
        self.text_doc.save()

        self.assertEqual("world", self.text_doc.text)
        self.assertEqual(1, TextDocument.objects.count())
