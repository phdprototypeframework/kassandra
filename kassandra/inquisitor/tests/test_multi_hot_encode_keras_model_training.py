from io import StringIO
from unittest.mock import patch

from django.core.management import call_command
from django.test import TestCase
from django.utils import timezone

import numpy as np
import pandas as pd

TRAIN_KERAS_MODEL_MODULE = (
    "kassandra.inquisitor.management.commands.train_multi_hot_encode_keras_models"
)


class TrainMultiHotEncodeKerasModel(TestCase):
    def run_management_command(self):
        stdout = StringIO()
        call_command(
            "train_multi_hot_encode_keras_models",
            "-m",
            "multi_hot_encode_model_file",
            "-k",
            "keras_model_file",
            "-t",
            "training_file1,training_file2",
            stdout=stdout,
        )

        return stdout.getvalue()

    @patch(f"{TRAIN_KERAS_MODEL_MODULE}.Command.train_keras_model")
    @patch(f"{TRAIN_KERAS_MODEL_MODULE}.Command.create_keras_model")
    @patch(f"{TRAIN_KERAS_MODEL_MODULE}.Command.bag_of_words_matrix")
    @patch(f"{TRAIN_KERAS_MODEL_MODULE}.Command.train_test_split")
    @patch(f"{TRAIN_KERAS_MODEL_MODULE}.Command.multi_hot_encode_tags")
    @patch(f"{TRAIN_KERAS_MODEL_MODULE}.Command.nlp_analysis")
    @patch(f"{TRAIN_KERAS_MODEL_MODULE}.Command.extract_training_file_data")
    def test_management_command(
        self,
        mock_extract_training_file_data,
        mock_nlp_analysis,
        mock_multi_hot_encode_tags,
        mock_train_test_split,
        mock_bag_of_words_matrix,
        mock_create_keras_model,
        mock_train_keras_model,
    ):
        mock_training_reviews = mock_extract_training_file_data.return_value = [
            {"tags": "run", "review_text": "Running shoes"},
            {"tags": "unknown", "review_text": "hello world"},
            {"tags": "walk", "review_text": "walking shoes"},
        ]
        mock_processed_reviews = mock_nlp_analysis.return_value = pd.DataFrame(
            [
                {"tags": "run", "review_text": "run shoe"},
                {"tags": "unknown", "review_text": "hello world"},
                {"tags": "walk", "review_text": "walk shoe"},
            ]
        )
        mock_encoded_tags = mock_multi_hot_encode_tags.return_value = np.array(
            [[1, 0, 0], [0, 0, 0], [0, 0, 1]]
        )

        (
            mock_training_data,
            mock_training_tags,
            mock_testing_data,
            mock_testing_tags,
        ) = mock_train_test_split.return_value = (
            np.array(["run shoes", "hello world"]),
            np.array([[1, 0, 0], [0, 0, 0]]),
            np.array(["walk shoe"]),
            np.array([[0, 0, 1]]),
        )

        (
            mock_training_matrix,
            mock_testing_matrix,
        ) = mock_bag_of_words_matrix.return_value = (
            np.array([[1.0, 0.0, 0.0], [0.0, 0.0, 0.0]]),
            np.array([[0.0, 0.0, 1.0]]),
        )

        mock_keras_model = mock_create_keras_model.return_value
        today = str(timezone.now().date())

        stdout = self.run_management_command()

        mock_extract_training_file_data.assert_called_once_with(
            ["training_file1", "training_file2"],
        )
        mock_nlp_analysis.assert_called_once_with(mock_training_reviews)
        mock_multi_hot_encode_tags.assert_called_once_with(mock_processed_reviews)
        mock_train_test_split.assert_called_once_with(
            mock_processed_reviews, mock_encoded_tags
        )
        mock_bag_of_words_matrix.assert_called_once_with(
            f"model_files/{today}-multi_hot_encode_model_file",
            mock_training_data,
            mock_testing_data,
        )
        mock_create_keras_model.assert_called_once_with(len(mock_encoded_tags[0]))
        mock_train_keras_model(
            mock_keras_model,
            mock_training_matrix,
            mock_testing_matrix,
            mock_training_tags,
            mock_testing_matrix,
            f"model_files/{today}-keras_model_file",
        )

        self.assertIn("Number of training reviews: 3", stdout)
        self.assertIn("Number of tags: {}".format(len(mock_encoded_tags[0])), stdout)
        self.assertIn("Keras model summary", stdout)
