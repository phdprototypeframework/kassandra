from unittest.mock import mock_open, patch

from django.test import TestCase

import numpy as np

from kassandra.inquisitor.classifiers import CustomModelPrediction, TextPreprocessor

INQUISITOR_CLASSIFIERS_MODULE = "kassandra.inquisitor.classifiers"


class TextPreprocessorTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.vocab_size = 10

    def test_text_preprocessor_instantiation(self):
        text_preprocessor = TextPreprocessor(self.vocab_size)

        self.assertEqual(self.vocab_size, text_preprocessor.vocab_size)
        self.assertEqual(None, text_preprocessor.tokenizer)

    @patch(f"{INQUISITOR_CLASSIFIERS_MODULE}.text")
    def test_create_tokenizer(self, mock_keras_text):
        mock_tokenizer = mock_keras_text.Tokenizer.return_value

        text_list = np.array(["run shoes", "hello world"])

        text_preprocessor = TextPreprocessor(self.vocab_size)
        text_preprocessor.create_tokenizer(text_list)

        self.assertEqual(self.vocab_size, text_preprocessor.vocab_size)
        self.assertEqual(mock_tokenizer, text_preprocessor.tokenizer)

        mock_keras_text.Tokenizer.assert_called_once_with(num_words=self.vocab_size)
        mock_tokenizer.fit_on_texts.assert_called_once_with(text_list)

    @patch(f"{INQUISITOR_CLASSIFIERS_MODULE}.text")
    def test_transform_text(self, mock_keras_text):
        expected_matrix = np.array([[1.0, 0.0, 0.0], [0.0, 0.0, 0.0]])
        mock_tokenizer = mock_keras_text.Tokenizer.return_value
        mock_tokenizer.texts_to_matrix.return_value = expected_matrix

        text_list = np.array(["run shoes", "hello world"])

        text_preprocessor = TextPreprocessor(self.vocab_size)
        text_preprocessor.create_tokenizer(text_list)

        data_matrix = text_preprocessor.transform_text(text_list)

        self.assertEqual(self.vocab_size, text_preprocessor.vocab_size)
        self.assertEqual(mock_tokenizer, text_preprocessor.tokenizer)
        mock_keras_text.Tokenizer.assert_called_once_with(num_words=self.vocab_size)
        mock_tokenizer.fit_on_texts.assert_called_once_with(text_list)

        self.assertTrue((expected_matrix == data_matrix).all())
        mock_tokenizer.texts_to_matrix.assert_called_once_with(text_list)


class CustomModelPredictionTestCase(TestCase):
    @patch(f"{INQUISITOR_CLASSIFIERS_MODULE}.pickle.load")
    @patch(f"{INQUISITOR_CLASSIFIERS_MODULE}.open", mock_open(read_data="hey"))
    @patch(f"{INQUISITOR_CLASSIFIERS_MODULE}.keras")
    def test_custom_model_prediction_instantiation(self, mock_keras, mock_pickle_load):
        mock_keras_model = mock_keras.models.load_model.return_value
        mock_processor_model = mock_pickle_load.return_value

        classifier = CustomModelPrediction.from_path(
            "multi_hot_encode_model_file", "keras_model_file"
        )

        self.assertEqual(mock_keras_model, classifier.model)
        self.assertEqual(mock_processor_model, classifier.processor)

        mock_keras.models.load_model.assert_called_once_with("keras_model_file")

    @patch(f"{INQUISITOR_CLASSIFIERS_MODULE}.keras")
    def test_custom_model_prediction_instantiation_fail(self, mock_keras):
        with self.assertRaises(FileNotFoundError):
            CustomModelPrediction.from_path(
                "keras_model_file", "multi_hot_encode_model_file"
            )

    @patch(f"{INQUISITOR_CLASSIFIERS_MODULE}.pickle.load")
    @patch(f"{INQUISITOR_CLASSIFIERS_MODULE}.open", mock_open(read_data="hey"))
    @patch(f"{INQUISITOR_CLASSIFIERS_MODULE}.keras")
    def test_predict(self, mock_keras, mock_pickle_load):
        mock_keras_model = mock_keras.models.load_model.return_value
        mock_processor_model = mock_pickle_load.return_value

        mock_preprocessed_data = mock_processor_model.transform_text.return_value

        expected_predictions = np.array([[0.65, 0.1]])
        mock_keras_model.predict.return_value = expected_predictions

        classifier = CustomModelPrediction.from_path(
            "multi_hot_encode_model_file", "keras_model_file"
        )

        instances = ["walk shoe"]
        predictions = classifier.predict(instances)

        self.assertEqual(mock_keras_model, classifier.model)
        self.assertEqual(mock_processor_model, classifier.processor)
        self.assertEqual(expected_predictions.tolist(), predictions)

        mock_keras.models.load_model.assert_called_once_with("keras_model_file")
        mock_processor_model.transform_text.assert_called_once_with(instances)
        mock_keras_model.predict.assert_called_once_with(mock_preprocessed_data)
