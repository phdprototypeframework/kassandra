import factory

from kassandra.inquisitor.models import PredictedUseCase, TextDocument


class PredictedUseCaseFactory(factory.DjangoModelFactory):
    class Meta:
        model = PredictedUseCase
        django_get_or_create = ("name",)

    name = factory.Sequence(lambda d: "predicted-use-case-%s" % d)


class TextDocumentFactory(factory.DjangoModelFactory):
    class Meta:
        model = TextDocument

    text = factory.Sequence(lambda d: "text-document-%s" % d)
    predicted_use_case = factory.SubFactory(PredictedUseCaseFactory)
