import logging
from unittest.mock import Mock, mock_open, patch

from django.shortcuts import reverse
from django.test import Client, TestCase

import numpy as np

from kassandra.inquisitor.views import (
    NO_PREDICTIONS_ERROR_MESSAGE,
    multi_hot_encode_tags,
    nlp_analysis,
    predict_use_case,
)

from .factory_fixtures import TextDocumentFactory

logging.disable(logging.CRITICAL)

INQUISITOR_VIEWS_MODULE = "kassandra.inquisitor.views"


class PredictionsTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.client = Client()

        cls.text_doc = TextDocumentFactory(text="Running shoes")

    @patch(f"{INQUISITOR_VIEWS_MODULE}.predict_use_case")
    @patch(f"{INQUISITOR_VIEWS_MODULE}.CustomModelPrediction")
    @patch(f"{INQUISITOR_VIEWS_MODULE}.nlp_analysis")
    @patch(f"{INQUISITOR_VIEWS_MODULE}.multi_hot_encode_tags")
    @patch(f"{INQUISITOR_VIEWS_MODULE}.os_path")
    @patch(
        f"{INQUISITOR_VIEWS_MODULE}.os_environ",
        {
            "KERAS_MODEL_FILE": "keras_model_file",
            "MULTI_HOT_ENCODE_MODEL_FILE": "multi_hot_encode_model_file",
            "TAGS_LIST_FILE": "tags_list_file",
        },
    )
    def test_predictions(
        self,
        mock_os_path,
        mock_multi_hot_encode_tags,
        mock_nlp_analysis,
        mock_custom_model_prediction,
        mock_predict_use_case,
    ):
        expected_predictions = [[0.7, 0.4, 0.2]]

        mock_tag_encoder = mock_multi_hot_encode_tags.return_value
        mock_tag_encoder.classes_.return_value = np.array(["run", "walk", "unknown"])

        mock_nlp_analysis.return_value = "run shoe"

        mock_classifier = mock_custom_model_prediction.from_path.return_value
        mock_classifier.predict.return_value = expected_predictions

        mock_predict_use_case.return_value = "run"

        response = self.client.get(
            reverse("inquisitor:predictions", args=(self.text_doc.id,))
        )

        self.assertEqual(200, response.status_code)

        content = response.content.decode("utf8")

        expected_review_text_in_html = "<td>Running shoes</td>"
        expected_predicted_use_case_in_html = "<td>run</td>"
        expected_button_in_html = f"""
        <a class="btn btn-primary" role="button"
            data-toggle="modal"
            data-target="#myModal_{self.text_doc.id}">
            Decide
        </a>
        """

        mock_multi_hot_encode_tags.assert_called_with("tags_list_file")
        mock_nlp_analysis.assert_called_with("Running shoes")
        mock_custom_model_prediction.from_path.assert_called_with(
            "multi_hot_encode_model_file", "keras_model_file"
        )
        mock_classifier.predict.assert_called_with(["run shoe"])
        mock_predict_use_case.assert_called_with(expected_predictions, mock_tag_encoder)

        self.assertInHTML(expected_review_text_in_html, content)
        self.assertInHTML(expected_predicted_use_case_in_html, content)
        self.assertInHTML(expected_button_in_html, content)

    @patch(
        f"{INQUISITOR_VIEWS_MODULE}.os_environ",
        {
            "KERAS_MODEL_FILE": "keras_model_file",
            "MULTI_HOT_ENCODE_MODEL_FILE": "multi_hot_encode_model_file",
            "TAGS_LIST_FILE": "tags_list_file",
        },
    )
    def test_predictions_internal_server_error(self):
        response = self.client.get(
            reverse("inquisitor:predictions", args=(self.text_doc.id,))
        )

        self.assertEqual(500, response.status_code)
        self.assertEqual(
            response.content.decode("utf8"),
            "Unexpected error. Please contact system administrator",
        )

    @patch(
        f"{INQUISITOR_VIEWS_MODULE}.os_environ",
        {
            "MULTI_HOT_ENCODE_MODEL_FILE": "multi_hot_encode_model_file",
            "TAGS_LIST_FILE": "tags_list_file",
        },
    )
    def test_predictions_keras_model_file_not_set(self):
        response = self.client.get(
            reverse("inquisitor:predictions", args=(self.text_doc.id,))
        )

        self.assertEqual(200, response.status_code)
        self.assertContains(response, NO_PREDICTIONS_ERROR_MESSAGE)

    @patch(
        f"{INQUISITOR_VIEWS_MODULE}.os_environ",
        {"KERAS_MODEL_FILE": "keras_model_file", "TAGS_LIST_FILE": "tags_list_file"},
    )
    def test_predictions_multi_hot_encode_model_file_not_set(self):
        response = self.client.get(
            reverse("inquisitor:predictions", args=(self.text_doc.id,))
        )

        self.assertEqual(200, response.status_code)
        self.assertContains(response, NO_PREDICTIONS_ERROR_MESSAGE)

    @patch(
        f"{INQUISITOR_VIEWS_MODULE}.os_environ",
        {
            "KERAS_MODEL_FILE": "keras_model_file",
            "MULTI_HOT_ENCODE_MODEL_FILE": "multi_hot_encode_model_file",
        },
    )
    def test_predictions_tags_list_file_not_set(self):
        response = self.client.get(
            reverse("inquisitor:predictions", args=(self.text_doc.id,))
        )

        self.assertEqual(200, response.status_code)
        self.assertContains(response, NO_PREDICTIONS_ERROR_MESSAGE)

    @patch(f"{INQUISITOR_VIEWS_MODULE}.MultiLabelBinarizer")
    @patch(f"{INQUISITOR_VIEWS_MODULE}.json.load")
    @patch(f"{INQUISITOR_VIEWS_MODULE}.open", mock_open(read_data="hey"))
    def test_muti_hot_encode_tags(self, mock_json_load, mock_tag_encoder):
        expected_tags_list = [["climb"], ["walk"]]

        mock_json_load.return_value = {"tags_list": expected_tags_list}

        tag_encoder = multi_hot_encode_tags("tags_list_file")

        self.assertEqual(mock_tag_encoder.return_value, tag_encoder)

        mock_tag_encoder.return_value.fit_transform.assert_called_once_with(
            expected_tags_list
        )

    @patch(f"{INQUISITOR_VIEWS_MODULE}.MultiLabelBinarizer")
    @patch(f"{INQUISITOR_VIEWS_MODULE}.json.load")
    def test_muti_hot_encode_tags_invalid_file(self, mock_json_load, mock_tag_encoder):
        with self.assertRaises(FileNotFoundError):
            multi_hot_encode_tags("tags_list_file")

    @patch(f"{INQUISITOR_VIEWS_MODULE}.Review")
    def test_nlp_analysis(self, mock_review):
        mock_review_features = mock_review.return_value.features = ["running", "shoes"]
        mock_review.return_value.lemmatize.return_value = ["run", "shoe"]

        text = "Running shoes"
        features = nlp_analysis(text)

        self.assertEqual("run shoe", features)

        mock_review.assert_called_once_with(text, preprocess=True)
        mock_review.return_value.lemmatize.assert_called_once_with(mock_review_features)

    def test_predict_use_case_success(self):
        tag_encoder = Mock()
        tag_encoder.classes_ = ["walk", "climb", "hike"]

        predictions = [[0.4, 0.75, 0.239]]

        predicted_use_case = predict_use_case(predictions, tag_encoder)

        self.assertEqual("climb", predicted_use_case)

    def test_predict_use_case_fail(self):
        tag_encoder = Mock()
        tag_encoder.classes_ = ["walk", "climb", "hike"]

        predictions = [[0.4, 0.57, 0.239]]

        predicted_use_case = predict_use_case(predictions, tag_encoder)

        self.assertEqual("unknown", predicted_use_case)


class StorePredictionsTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.client = Client()

        cls.text_document = TextDocumentFactory(
            text="Running shoes", predicted_use_case=None
        )

    def test_store_predictions_accurate_use_case_prediction(self):
        response = self.client.get(
            "{}?use_case=run&accurate=true".format(
                reverse("inquisitor:store_predictions", args=(self.text_document.id,)),
            ),
        )

        self.text_document.refresh_from_db()

        self.assertEqual(302, response.status_code)
        self.assertEqual("/documents", response.url)
        self.assertEqual("run", self.text_document.predicted_use_case.name)
        self.assertEqual(True, self.text_document.accurate_use_case)

    def test_store_predictions_inaccurate_use_case_prediction(self):
        response = self.client.get(
            "{}?use_case=unknown&accurate=false".format(
                reverse("inquisitor:store_predictions", args=(self.text_document.id,)),
            ),
        )

        self.text_document.refresh_from_db()

        self.assertEqual(302, response.status_code)
        self.assertEqual("/documents", response.url)
        self.assertEqual("unknown", self.text_document.predicted_use_case.name)
        self.assertEqual(False, self.text_document.accurate_use_case)

    def test_store_predictions_missing_use_case_key(self):
        response = self.client.get(
            "{}?accurate=true".format(
                reverse("inquisitor:store_predictions", args=(self.text_document.id,)),
            ),
        )

        self.assertEqual(400, response.status_code)

    def test_store_predictions_missing_accurate_key(self):
        response = self.client.get(
            "{}?use_case=run".format(
                reverse("inquisitor:store_predictions", args=(self.text_document.id,)),
            ),
        )

        self.assertEqual(400, response.status_code)


class ExpectedPredictionTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.client = Client()

        cls.text_document = TextDocumentFactory(
            text="Running shoes", accurate_use_case=False
        )

    def test_expected_prediction_get_request(self):
        response = self.client.get(
            reverse("inquisitor:expected_prediction", args=(self.text_document.id,))
        )

        self.assertEqual(200, response.status_code)
        self.assertContains(response, "Provide the expected prediction for this text")

    def test_expected_prediction_post_success(self):
        response = self.client.post(
            reverse("inquisitor:expected_prediction", args=(self.text_document.id,)),
            {"text": self.text_document.text, "expected_use_case": "run"},
        )

        self.text_document.refresh_from_db()

        self.assertEqual(302, response.status_code)
        self.assertEqual("/documents", response.url)
        self.assertEqual("run", self.text_document.expected_use_case)

    def test_expected_prediction_post_fail_expected_prediction_blank(self):
        response = self.client.post(
            reverse("inquisitor:expected_prediction", args=(self.text_document.id,)),
            {"text": self.text_document.text, "expected_use_case": ""},
        )

        self.assertEqual(200, response.status_code)
        self.assertContains(response, "Please provide an expected prediction")

    def test_expected_prediction_post_fail_no_expected_prediction(self):
        response = self.client.post(
            reverse("inquisitor:expected_prediction", args=(self.text_document.id,)),
            {"text": self.text_document.text},
        )

        self.assertEqual(200, response.status_code)
        self.assertContains(response, "Please provide an expected prediction")
