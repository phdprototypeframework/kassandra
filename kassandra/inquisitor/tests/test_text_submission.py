from django.shortcuts import reverse
from django.test import Client, TestCase

from kassandra.inquisitor.models import TextDocument

from .factory_fixtures import PredictedUseCaseFactory, TextDocumentFactory


class TextSubmissionTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.client = Client()

    def test_text_document_submission_get_request(self):
        response = self.client.get(reverse("inquisitor:index"))

        self.assertEqual(200, response.status_code)
        self.assertContains(
            response, "In greek mythology, Kassandra was a woman who uttered accurate"
        )

    def test_text_document_submission_post_success(self):
        response = self.client.post(
            reverse("inquisitor:index"), {"text": "Hello World"},
        )

        text_doc = TextDocument.objects.first()

        self.assertEqual(302, response.status_code)
        self.assertEqual(f"/predictions/{text_doc.id}", response.url)


class TextDocumentTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.text_document = TextDocumentFactory(
            text="Running shoes", predicted_use_case=None
        )

    def test_text_documents_dashboard_empty(self):
        TextDocument.objects.all().delete()

        response = self.client.get(reverse("inquisitor:text_documents"))

        self.assertEqual(200, response.status_code)
        self.assertContains(response, "No text documents available at this time")

    def test_text_documents_dashboard_no_predictions(self):
        response = self.client.get(reverse("inquisitor:text_documents"))

        expected_in_html = """
        <td >Running shoes</td>
        <td >No Prediction</td>
        <td >Not set</td>
        <td ></td>
        <td ><a class='btn btn-outline-primary' href='/predictions/{}'>Predict</a></td>
        """.format(
            self.text_document.id,
        )

        self.assertEqual(200, response.status_code)
        self.assertInHTML(expected_in_html, response.content.decode("utf8"))

    def test_text_documents_dashboard_with_predictions(self):
        self.text_document.predicted_use_case = PredictedUseCaseFactory(name="run")
        self.text_document.accurate_use_case = True
        self.text_document.save()

        response = self.client.get(reverse("inquisitor:text_documents"))

        expected_in_html = """
        <td >Running shoes</td>
        <td >run</td>
        <td >Yes</td>
        <td ></td>
        <td ></td>
        """

        self.assertEqual(200, response.status_code)
        self.assertInHTML(expected_in_html, response.content.decode("utf8"))

    def test_text_documents_dashboard_with_inaccurate_predictions(self):
        self.text_document.predicted_use_case = PredictedUseCaseFactory(name="run")
        self.text_document.accurate_use_case = False
        self.text_document.save()

        response = self.client.get(reverse("inquisitor:text_documents"))

        expected_in_html = """
        <td >Running shoes</td>
        <td >run</td>
        <td >No</td>
        <td ><a class='btn btn-outline-dark' href='/predictions/expected/{}'>
        Submit</a></td>
        <td ></td>
        """.format(
            self.text_document.id,
        )

        self.assertEqual(200, response.status_code)
        self.assertInHTML(expected_in_html, response.content.decode("utf8"))

    def test_text_documents_dashboard_with_expected_prediction(self):
        self.text_document.predicted_use_case = PredictedUseCaseFactory(name="run")
        self.text_document.accurate_use_case = False
        self.text_document.expected_use_case = "walk"
        self.text_document.save()

        response = self.client.get(reverse("inquisitor:text_documents"))

        expected_in_html = """
        <td >Running shoes</td>
        <td >run</td>
        <td >No</td>
        <td >walk</td>
        <td ></td>
        """

        self.assertEqual(200, response.status_code)
        self.assertInHTML(expected_in_html, response.content.decode("utf8"))
