import json
import pickle
import string
from os import environ as os_environ, path as os_path

from django.core.management.base import BaseCommand
from django.utils import timezone

import pandas as pd
from review_analyzer import Review
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.utils import shuffle
from tensorflow.keras.layers import Dense
from tensorflow.keras.metrics import Precision, Recall
from tensorflow.keras.models import Sequential

from kassandra.inquisitor.classifiers import TextPreprocessor


class Command(BaseCommand):
    def add_arguments(self, parser):
        """ Arguments passed via the command line
        """
        parser.add_argument(
            "-t",
            "--training-files",
            type=str,
            help=(
                "Name of files containing training data. "
                "One or more files can be provided e.g. file1,file2"
            ),
        )
        parser.add_argument(
            "-m",
            "--multi-hot-encode-model-file",
            type=str,
            help="Name of file to save multi hot encode model to.",
        )
        parser.add_argument(
            "-k",
            "--keras-model-file",
            type=str,
            help="Name of file to save keras model to.",
        )

    def handle(self, *args, **options):
        """ Train keras model using bag of words and
        multi hot encoding
        """
        today = str(timezone.now().date())
        model_files_dir = os_environ.get("MODEL_FILES_DIR", "model_files")
        training_files = options["training_files"].split(",")
        multi_hot_encode_model_file = os_path.join(
            model_files_dir,
            "{0}-{1}".format(today, options["multi_hot_encode_model_file"]),
        )
        keras_model_file = os_path.join(
            model_files_dir, "{0}-{1}".format(today, options["keras_model_file"])
        )

        training_reviews = self.extract_training_file_data(training_files)
        self.stdout.write("Number of training reviews: %d" % len(training_reviews))

        processed_reviews = self.nlp_analysis(training_reviews)
        encoded_tags = self.multi_hot_encode_tags(processed_reviews)

        (
            training_data,
            training_tags,
            testing_data,
            testing_tags,
        ) = self.train_test_split(processed_reviews, encoded_tags)

        training_matrix, testing_matrix = self.bag_of_words_matrix(
            multi_hot_encode_model_file, training_data, testing_data
        )

        num_tags = len(encoded_tags[0])
        self.stdout.write("Number of tags: %d" % num_tags)

        keras_model = self.create_keras_model(num_tags)
        self.stdout.write("Keras model summary")
        keras_model.summary()

        self.train_keras_model(
            keras_model,
            training_matrix,
            testing_matrix,
            training_tags,
            testing_tags,
            keras_model_file,
        )

    def extract_training_file_data(self, training_files):
        """ Extract data from training files
        """
        training_reviews = []
        for training_file in training_files:
            try:
                with open(training_file, "r") as fhandle:
                    training_reviews.extend(json.load(fhandle)[:5000])
            except FileNotFoundError:
                self.stderr.write("Training file '%s' not found" % training_file)
                continue

        return shuffle(training_reviews, random_state=22)

    def nlp_analysis(self, training_reviews):
        """ Perform Natural Language Processing (NLP) on
        training reviews
        """
        processed_reviews = []
        for row in training_reviews:
            tags = row["tags"]
            text = row["review_text"]
            review_text = text.translate(str.maketrans("", "", string.punctuation))

            review = Review(review_text, preprocess=True)
            features = " ".join(review.lemmatize(review.features))

            processed_reviews.append({"tags": tags, "review_text": features})

        return pd.DataFrame(processed_reviews)

    def multi_hot_encode_tags(self, processed_reviews):
        """ Multi hot encode training reviews tags
        """
        tags_list = [tags.split(",") for tags in processed_reviews["tags"].values]
        tags_list_dict = {"tags_list": tags_list}
        # save tags list to file for re-use later
        tags_list_filename = "model_files/{}-tags-list.json".format(
            str(timezone.now().date())
        )
        json.dump(tags_list_dict, open(tags_list_filename, "w"))
        self.stdout.write("Tags list written to '%s'" % tags_list_filename)

        tag_encoder = MultiLabelBinarizer()

        return tag_encoder.fit_transform(tags_list)

    def train_test_split(self, processed_reviews, encoded_tags):
        """ Split data into training and testing datasets.
        90% will be used to train models, 10% will be used to test models
        """
        training_size = int(len(processed_reviews) * 0.9)
        testing_size = len(processed_reviews) - training_size
        self.stdout.write("Size of training data: %d" % training_size)
        self.stdout.write("Size of testing data: %d" % testing_size)

        training_data = processed_reviews["review_text"].values[:training_size]
        training_tags = encoded_tags[:training_size]
        testing_data = processed_reviews["review_text"].values[training_size:]
        testing_tags = encoded_tags[training_size:]

        return training_data, training_tags, testing_data, testing_tags

    def bag_of_words_matrix(
        self, multi_hot_encode_model_file, training_data, testing_data, vocab_size=1000
    ):
        """ Create bag of words matrix
        """
        text_processor = TextPreprocessor(vocab_size)
        text_processor.create_tokenizer(training_data)

        training_matrix = text_processor.transform_text(training_data)
        testing_matrix = text_processor.transform_text(testing_data)

        self.stdout.write(
            "Save TextProcessor model: '%s'" % multi_hot_encode_model_file
        )
        with open(multi_hot_encode_model_file, "wb") as fhandle:
            pickle.dump(text_processor, fhandle)

        return training_matrix, testing_matrix

    def create_keras_model(self, num_tags, vocab_size=1000):
        """ Create keras sequential model
        """
        model = Sequential()
        model.add(
            Dense(50, input_shape=(vocab_size,), activation="relu")
        )  # hidden layer
        model.add(Dense(25, activation="relu"))  # hidden layer
        model.add(Dense(num_tags, activation="sigmoid"))

        model.compile(
            loss="binary_crossentropy",
            optimizer="adam",
            metrics=["accuracy", Precision(), Recall()],
        )
        return model

    def train_keras_model(
        self,
        keras_model,
        training_matrix,
        testing_matrix,
        training_tags,
        testing_tags,
        keras_model_file,
    ):
        """ Train and evaluate keras model
        """
        self.stdout.write("Train keras model")
        keras_model.fit(
            training_matrix,
            training_tags,
            epochs=5,
            batch_size=128,
            validation_split=0.1,
        )
        self.stdout.write("Evaluate keras model")
        keras_model.evaluate(testing_matrix, testing_tags, batch_size=128)
        self.stdout.write("Save keras model: '%s'" % keras_model_file)
        keras_model.save(keras_model_file)
