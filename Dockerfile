FROM python:3.7.7-slim-buster
# The enviroment variable ensures that the python output is set straight
# to the terminal with out buffering it first
ENV PYTHONUNBUFFERED 1

# runtime system dependencies
RUN apt-get update \
        && apt-get install -y --no-install-recommends \
        git build-essential pkg-config libpq-dev postgresql-client \
        && rm -rf var/lib/apt/lists/*

COPY ./dependencies/* requirements.txt download_nltk_stopwords.py /
# Install app dependencies
RUN pip install /review_analyzer-0.3.0-py3-none-any.whl psycopg2~=2.8.5 factory-boy
RUN pip install -r /requirements.txt
# Download NLTK stopwords and TextBlob corpora
RUN python /download_nltk_stopwords.py
RUN python -m textblob.download_corpora

RUN mkdir /code

COPY kassandra /code/kassandra
COPY model_files /code/model_files
COPY manage.py \
        setup.py \
        setup.cfg \
        pyproject.toml \
        docker-entrypoint \
        /code/

WORKDIR /code

ENTRYPOINT ["/code/docker-entrypoint"]

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
