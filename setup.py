from setuptools import find_packages, setup

setup(
    name="kassandra",
    use_scm_version=True,
    setup_requires=["setuptools_scm"],
    packages=find_packages(exclude=["kassandra.inquisitor.tests"]),
    python_requires=">=3.7",
    install_requires=[
        "Django~=3.0",
        "django-bootstrap4",
        "django-extensions",
        "django-tables2",
        "django-widget-tweaks",
        "dj-database-url",
        "numpy~=1.16.0",
        "pandas",
        "scikit-learn~=0.22.2",
        "tensorflow~=1.14.0",
    ],
    extras_require={
        "dev": ["django-debug-toolbar"],
        "psycopg2": ["psycopg2"],
        "psycopg2-binary": ["psycopg2-binary"],
        "testing": ["factory-boy"],
    },
    scripts=["manage.py"],
)
