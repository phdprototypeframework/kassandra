SHELL := /bin/bash

USER_ID := $(shell id -u)
GROUP_ID := $(shell id -g)
APP_NAME := kassandra
APP_VERSION := $(shell git describe --tags --dirty --always)
POSTGRES_VERSION=12.2

.PHONY: all
all: build

.PHONY: build
build:
	@echo "[Building image]"
	docker build --tag $(APP_NAME):$(APP_VERSION) .

.PHONY: up
up: build
	UID=$(USER_ID) APP_VERSION=$(APP_VERSION) docker-compose up -d

.PHONY: down
down:
	docker-compose down

.PHONY: build-dists
build-dists:
	tox -e build-dists

.PHONY: test
test: build
	@echo "[Running unit tests]"
	docker pull postgres:$(POSTGRES_VERSION)

	$(eval POSTGRES_CONTAINER := $(shell \
	docker run \
	-e POSTGRES_DB=$(APP_NAME) \
	-e POSTGRES_USER=$(DB_USER) \
	-e POSTGRES_PASSWORD=$(DB_PASSWORD) \
	-p 5432:5432 \
	-d postgres:$(POSTGRES_VERSION)))

	docker run --rm --user "$(USER_ID):$(GROUP_ID)" \
	-e DATABASE_URL=postgres://$(DB_USER):$(DB_PASSWORD)@db/$(APP_NAME) \
	-e DJANGO_SETTINGS_MODULE="kassandra.settings.docker" \
	--link $(POSTGRES_CONTAINER):db \
	$(APP_NAME):$(APP_VERSION) \
	./manage.py test --parallel \
	|| (echo "docker rm -f [postgres]" \
	&& docker rm -f $(POSTGRES_CONTAINER) && false)

	@echo "docker rm -f [postgres]"
	docker rm -f $(POSTGRES_CONTAINER)

.PHONY: check
check:
	@echo "Running QA checks"
	tox -e checkqa

.PHONY: requirements
requirements:
	pip-compile --generate-hashes --allow-unsafe

	pip-compile --generate-hashes --allow-unsafe \
	--output-file requirements-dev.txt requirements-dev.in

.PHONY: upgrade-requirements
upgrade-requirements:
	pip-compile --upgrade --generate-hashes --allow-unsafe

	pip-compile --upgrade --generate-hashes --allow-unsafe \
	--output-file requirements-dev.txt requirements-dev.in

.PHONY: sync-requirements
sync-requirements:
	pip-sync requirements-dev.txt
