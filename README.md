# Kassandra

[![pipeline status](https://gitlab.com/phdprototypeframework/kassandra/badges/master/pipeline.svg)](https://gitlab.com/phdprototypeframework/kassandra/-/commits/master)
[![coverage report](https://gitlab.com/phdprototypeframework/kassandra/badges/master/coverage.svg)](https://gitlab.com/phdprototypeframework/kassandra/-/commits/master)

In greek mythology, Kassandra was a woman who uttered accurate
prophecies, but was never believed.

This is a web application that analyses text to identify and predict use cases
such as run, walk, swim, etc.

## Technology Stack

- Docker
- Postgres
- Python3
- Django
- Keras
- NLTK
- TextBlob
- NLP
- Text Classification

## Development in Virtual Environment

Create and activate a virtual environment.

Install development requirements: `pip install -r requirements-dev.txt`

Install `review-analyzer` dependency:

```
pip install dependencies/review_analyzer-0.3.0-py3-none-any.whl
```

This is a local dependency required by Kassandra to perform
Natural Language Processing (NLP) on text.

Run local server:`python manage.py runserver`

This project has a `setup.py` so you can install it into your environment
as if it were a python package.

```
pip install -e .
```

Run local server: `manage.py runserver`

## Development in Docker Environment

Build the docker containers for the application and postgres database

```
make up
```

Stop docker containers

```
make down
```

Running tests in similar manner to Gitlab CI pipeline

```
make test
```

Build docker image

```
make build
```

## Text Classification

For Kassandra to successfully analyse text and predict use cases,
it requires pre-trained Machine Learning models.

Store the pre-trained models in enviroment variables as described below:

```
export KERAS_MODEL_FILE=<file location>
export MULTI_HOT_ENCODE_MODEL_FILE=<file location>
export TAGS_LIST_FILE=<file location>
```

The `model_files` directory contains pre-trained models that can be used.

## Testing

`tox` is used test the application.

To run tests: `make test`

For more information: https://tox.readthedocs.io/en/latest/

## Code QA

`pre-commit` is used to run checks on the codebase before commits are
made to `git`.

To run checks: `make check`

You can invoke `pre-commit` directly:`pre-commit run --all-files`

For more information: https://pre-commit.com/

## Dependency Management

`pip-tools` is used to manage dependencies.

For more information: https://github.com/jazzband/pip-tools

## Continuous Integration

This is handled by Gitlab CI. See: `.gitlab-ci.yml` file
